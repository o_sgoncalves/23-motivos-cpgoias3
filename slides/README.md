---
marp: true
theme: uncover
author: Samuel Goncalves
paginate: false
backgroundColor: #2a1f4a
foregroundColor: #FFFFFF
color: #fff
colorSecondary: #a8095c
backgroundImage: url('images/bg.png')
style: |
    section{
      font-family: "Trebuchet MS", sans-serif, monospace;
    }
    section::after {
      font-weight: bold;
      content: attr(data-marpit-pagination) '/' attr(data-marpit-pagination-total);
      font-size: 13pt;
      color: #10b5bf;
      text-shadow: 1px 1px 0 #000;
    }  

---
<!-- _backgroundImage: url('images/bg-destaque.png') -->
<style scoped>
  p {
    font-size: 30pt;
    text-align: right;
  }
  blockquote {
    font-size: 24pt;
  }
  {
   font-size: 45px;
  }
  b {
   color: #fff 
  }
</style>
<!-- _paginate: false -->
# 23 motivos para usar Software Livre em 2023
Samuel Gonçalves

---
<!-- _paginate: false -->
<style scoped>
  p {
    font-size: 25pt;
    text-align: right;
  }
  {
   font-size: 39px;
  }
</style>
<!-- _class: lead -->
#
#
> ## *Liberdade implica responsabilidade, por isso tantos a temem!*

#
*Bernard Shaw*


---
<!-- _backgroundImage: url('images/bg-press.png') -->
<!-- _paginate: false -->
<style scoped>
  h4 {
    font-size: 35pt;
    list-style-type: circle;
    font-weight: 900;
    color: #e0c7d3
  }
  p {
    font-size: 13pt;
  }
  {
   font-size: 28px;
  }
  img[alt~="center"] {
    display: block;
    margin: 0 auto;
  }
  li {
    list-style-type: square;
  }
</style>

![bg right:40%](images/perfil.png)

#### 🇧🇷 Samuel Gonçalves

* Tech Lead na 4Linux
* Consultor de Tecnologia nas áreas **Segurança Ofensiva** e **DevSecOps**
* Mais de 10 anos de experiência 💻
* *"5k++"* alunos ensinados no 🌎
* Músico, Contista, YouTuber e Podcaster
* Apaixonado por Software Livre 💙

---
<!-- _backgroundImage: url('images/bg-clean.png') -->
<style scoped>
  p {
    font-size: 23pt;
    color: #a70052
  }
  h2 {
   font-size: 55px;
  }
</style>
## Entre em contato
[https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)
![bg left:50% 70%](images/qrcode-contato-white.png)

---
<style scoped>
  p {
    font-size: 23pt;
    color: #a70052
  }
  h2 {
   font-size: 55px;
  }
  h3 {
   font-size: 46px;
  }
</style>

## Estes slides são **OpenSource**!!
![bg right:50% 70%](images/qrcode-slides.jpg)

### Personalize e utilize!
---

### **SORTEIO 4LINUX**
![bg left:50% 80%](images/qr-sorteio.png)
##### Inscreva-se e concorra a uma assinatura com mais de 30 cursos!
Escaneie o QRCode!

---

## Por que Software Livre?

---
<style scoped>
  h1 {
    font-size: 70pt;
    color: #ff029c;
    text-shadow: #fff 6px 0 10px;
  }
  p {
   font-size: 45px;
  }
</style>

# #1
#
Você **pode** executar o programa como desejar, para **qualquer** propósito

---
<!-- _backgroundImage: url('images/bg-clean.png') -->
<style scoped>
  h1 {
    font-size: 70pt;
    color: #fff;
    text-shadow: #ff029c 10px 0 20px;
  }
  p {
   font-size: 45px;
  }
</style>
# #2
#
Você **pode** estudar como o programa funciona, e adaptá-lo às suas necessidades

---
<style scoped>
  h1 {
    font-size: 70pt;
    color: #ff029c;
    text-shadow: #fff 6px 0 10px;
  }
  p {
   font-size: 45px;
  }
</style>
# #3
#
Você **pode** redistribuir cópias do software livremente

---
<!-- _backgroundImage: url('images/bg-clean.png') -->
<style scoped>
  h1 {
    font-size: 70pt;
    color: #fff;
    text-shadow: #ff029c 10px 0 20px;
  }
  p {
   font-size: 45px;
  }
</style>
# #4
#
Você **pode** distribuir cópias de suas versões **modificadas**, de modo que possa **ajudar** outros

---
<style scoped>
  h1 {
    font-size: 70pt;
    color: #ff029c;
    text-shadow: #fff 6px 0 10px;
  }
  h2 {
    font-size: 25pt;
  }
  p {
   font-size: 45px;
  }
</style>
# #5
#
Ao contrário da maioria das profissões, o talento de código aberto é escasso e em alta demanda
## ***Obs.:** E está amplamente documentado na internet!*

---
<!-- _backgroundImage: url('images/bg-clean.png') -->
<style scoped>
  h1 {
    font-size: 70pt;
    color: #fff;
    text-shadow: #ff029c 10px 0 20px;
  }
  p {
   font-size: 45px;
  }
</style>
# #6
#
A cultura do OpenSource permite que equipes colaborem mais entre si, tornando o desenvolvimento mais ágil e seguro

---
<style scoped>
  h1 {
    font-size: 70pt;
    color: #ff029c;
    text-shadow: #fff 6px 0 10px;
  }
  p {
   font-size: 45px;
  }
</style>
# #7
#
Diversos países ao redor do Planeta tem adotado FOSS como padrão e incentivado seu uso na indústria

---
<!-- _backgroundImage: url('images/bg-clean.png') -->
<style scoped>
  h1 {
    font-size: 70pt;
    color: #fff;
    text-shadow: #ff029c 10px 0 20px;
  }
  p {
   font-size: 45px;
  }
</style>
# #8
#
Estratégias de modernização da infraestrutura constituem o uso principal do open source empresarial

---
<style scoped>
  h1 {
    font-size: 70pt;
    color: #ff029c;
    text-shadow: #fff 6px 0 10px;
  }
  p {
   font-size: 45px;
  }
</style>
# #9
#
O mercado global de serviços de código aberto deve crescer **18,2%** até 2026, movimentando mais de **US$ 50 bilhões** até 2026, e mais de **US$ 21,7 bilhões** ainda em 2021.

---
<!-- _backgroundImage: url('images/bg-clean.png') -->
<style scoped>
  h1 {
    font-size: 70pt;
    color: #fff;
    text-shadow: #ff029c 10px 0 20px;
  }
  p {
   font-size: 45px;
  }
</style>
# #10
#
Segundo a **RedHat** 90% das empresas líderes de TI usam soluções open source empresariais

---
<style scoped>
  h1 {
    font-size: 70pt;
    color: #ff029c;
    text-shadow: #fff 6px 0 10px;
  }
  p {
   font-size: 45px;
  }
</style>
# #11
# 
Software Livre **Estimula a inovação e cultiva uma melhor aprendizagem**

---
<!-- _backgroundImage: url('images/bg-clean.png') -->
<style scoped>
  h1 {
    font-size: 70pt;
    color: #fff;
    text-shadow: #ff029c 10px 0 20px;
  }
  p {
   font-size: 45px;
  }
</style>
# #12
**Pode** promover a inclusão digital de forma econômica e eficiente

---
<style scoped>
  h1 {
    font-size: 70pt;
    color: #ff029c;
    text-shadow: #fff 6px 0 10px;
  }
  p {
   font-size: 45px;
  }
</style>
# #13
# 
Possibilita a independência do fornecedor já que o contratante não fica dependente de apenas um fabricante, tão pouco obrigado a adquirir novas licenças a cada vez que algum software deixa de ter suporte ou lança-se uma nova versão

---
<!-- _backgroundImage: url('images/bg-clean.png') -->
<style scoped>
  h1 {
    font-size: 70pt;
    color: #fff;
    text-shadow: #ff029c 10px 0 20px;
  }
  p {
   font-size: 45px;
  }
</style>
# #14
#
Intensifica a segurança da informação, pois, com o software aberto é possível identificar e corrigir melhor possíveis falhas, de maneira mais rápida e eficiente

---
<style scoped>
  h1 {
    font-size: 70pt;
    color: #ff029c;
    text-shadow: #fff 6px 0 10px;
  }
  p {
   font-size: 45px;
  }
</style>
# #15
#
Softwares **OpenSource** são o **motor** das transformações digitais

---
<!-- _backgroundImage: url('images/bg-clean.png') -->
<style scoped>
  h1 {
    font-size: 70pt;
    color: #fff;
    text-shadow: #ff029c 10px 0 20px;
  }
  p {
   font-size: 45px;
  }
</style>
# #16
#
Áreas relacionadas a **edge computing** e **IA/ML** são grandes produtores e consumidores de tecnologia **Open Source**, com crescimento superior a 20%, previsto para os próximos 2 anos

---
<style scoped>
  h1 {
    font-size: 70pt;
    color: #ff029c;
    text-shadow: #fff 6px 0 10px;
  }
  p {
   font-size: 45px;
  }
</style>
# #17
#
O uso de tecnologias abertas para gerenciamento de containers cresce exponencialmente, podendo chegar a 72% nos próximos 2 anos

---
<!-- _backgroundImage: url('images/bg-clean.png') -->
<style scoped>
  h1 {
    font-size: 70pt;
    color: #fff;
    text-shadow: #ff029c 10px 0 20px;
  }
  p {
   font-size: 45px;
  }
</style>
# #18
#
Colaboração é parte integrante da vida humana, e graças a ela chegamos até aqui

---
<style scoped>
  h1 {
    font-size: 70pt;
    color: #ff029c;
    text-shadow: #fff 6px 0 10px;
  }
  p {
   font-size: 45px;
  }
</style>
# #19
#
Você **pode** ser protagonista desta história!

---
<!-- _backgroundImage: url('images/bg-clean.png') -->
<style scoped>
  h1 {
    font-size: 70pt;
    color: #fff;
    text-shadow: #ff029c 10px 0 20px;
  }
  p {
   font-size: 45px;
  }
</style>
# #20
#
Os processos tecnológicos podem se tornar muito mais transparentes e participativos

---
<style scoped>
  h1 {
    font-size: 70pt;
    color: #ff029c;
    text-shadow: #fff 6px 0 10px;
  }
  p {
   font-size: 45px;
  }
</style>
# #21
#
O software livre pode ser uma ferramenta valiosa para ensinar programação e promover o conhecimento em computação.

---
<!-- _backgroundImage: url('images/bg-clean.png') -->
<style scoped>
  h1 {
    font-size: 70pt;
    color: #fff;
    text-shadow: #ff029c 10px 0 20px;
  }
  p {
   font-size: 45px;
  }
</style>
# #22
#
A natureza aberta do software livre permite que você entenda como os programas funcionam e como os dados são manipulados, te dando controle total sobre suas informações

---
<style scoped>
  h1 {
    font-size: 70pt;
    color: #ff029c;
    text-shadow: #fff 6px 0 10px;
  }
  p {
   font-size: 45px;
  }
</style>
# #23
#
Comunidades de Software Livre criaram uma nova maneira de colaboração, descentralizada e sem a necessidade de coordenação, favorecendo a **democracia**!


---
<style scoped>
  p {
    font-size: 25pt;
    text-align: right;
  }
  {
   font-size: 39px;
  }
</style>
> ## Quanto mais ideias há em circulação, mais ideias há para qualquer indivíduo discordar delas.
*Clay Shirky*

---
<style scoped>
  h1 {
    font-size: 34pt;
  }
  p {
   font-size: 30px;
  }
</style>
# Recomendação

[Clay Shirky: Como a internet irá (um dia) transformar o governo](https://www.ted.com/talks/clay_shirky_how_the_internet_will_one_day_transform_government?language=pt-br)
![bg left:60%](images/ClayShirky.jpg)

---
<!-- _class: lead -->
## **Obrigado!**

Vamos nos conectar?
* **Site:** [sgoncalves.tec.br](https://sgoncalves.tec.br)
* **E-mail:** [samuel@sgoncalves.tec.br](https://sgoncalves.tec.br/contato)
* **Linkedin:** [linkedin.com/in/samuelgoncalvespereira/](linkedin.com/in/samuelgoncalvespereira/)
* **Telegram:** [t.me/Samuel_gp](t.me/Samuel_gp)
* **Todas as redes:** [https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)

---
<style scoped>
  p {
  font-size: 22pt;
  list-style-type: circle;;
}
</style>

### Referências Bibliográficas
[O que é software livre?](https://www.gnu.org/philosophy/free-sw.pt-br.html)
[O Estado do Open Source Empresarial](https://www.redhat.com/pt-br/enterprise-open-source-report/2021)
[Open Source: O motor que está alavancando o crescimento da cloud](https://www.ascenty.com/blog/artigos/open-source-o-motor-que-esta-alavancando-o-crescimento-da-cloud/)
[Global Open Source Services Market (2021 to 2026)](https://finance.yahoo.com/news/global-open-source-services-market-094500559.html?guccounter=1&guce_referrer=aHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS8&guce_referrer_sig=AQAAALNhEaJe700goKU_E7OvWQkb2d3CSWnK5qz03i4LLLKqlieRYxwslPkqxV494FM1K8z-pWAM44-I9HD0WZ-9M80rnkllxgh_phycu_66KzYTzeo-C_EpvpEFXUUxn-DwwRgqfqebKyBJCZpN4A-32BRP9VzxbxIMB6FwH_xKQaw6)
[Open Source: o crescimento sustentável no mercado da tecnologia](https://www.mjvinnovation.com/pt-br/blog/open-source/)
[Por que é importante que o poder público use software livre](https://brasil.elpais.com/brasil/2017/08/25/tecnologia/1503682398_611930.html)
