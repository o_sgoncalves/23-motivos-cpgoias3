# Slides da palestra **23 Motivos para usar Software Livre em 2023**

Este repositório armazena os slides utilizados na palestra intitulada **23 Motivos para usar Software Livre em 2023** na CPGOIAS3 2023, evento realizado em Goiânia - Goiás ministrado por mim, Samuel Gonçalves.

## whoami

### 🇧🇷 Samuel Gonçalves

* Tech Lead na 4Linux
* Consultor de Tecnologia nas áreas **Segurança Ofensiva** e **DevSecOps**
* Mais de 10 anos de experiência 💻
* *"5k++"* alunos ensinados no 🌎
* Músico, Contista, YouTuber e Podcaster
* Apaixonado por Software Livre 💙

> Contato: [https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)

## Slides

Os slides foram escritos utilizando Markdown e estilizados com **marp**.

Seguem links de referência:
* [https://github.com/marp-team/marp](https://github.com/marp-team/marp)
* [https://www.markdownguide.org/](https://www.markdownguide.org/)

Dentro do diretório "slides" existe um arquivo chamado "slides.pdf" já estilizado. 

[Você pode clicar aqui para realizar o download do arquivo diretamente, caso prefira.](https://gitlab.com/o_sgoncalves/23-motivos-cpgoias3/-/raw/main/slides/README.pdf?inline=false)
